package com.gitlab.quoopie.twitchplugin;

import com.gitlab.quoopie.twitchplugin.Commands.togglecommands.TwitchChatOffCommand;
import com.gitlab.quoopie.twitchplugin.Commands.togglecommands.TwitchChatOffTabCompleter;
import com.gitlab.quoopie.twitchplugin.Commands.togglecommands.TwitchChatOnCommand;
import com.gitlab.quoopie.twitchplugin.Commands.togglecommands.TwitchChatOnTabCompleter;
import com.gitlab.quoopie.twitchplugin.Commands.twitch.TwitchCommand;
import com.gitlab.quoopie.twitchplugin.Commands.twitch.TwitchTabCompleter;
import com.gitlab.quoopie.twitchplugin.Commands.twitchChat.TwitchChatCommand;
import com.gitlab.quoopie.twitchplugin.Commands.twitchChat.TwitchChatTabCompleter;
import com.gitlab.quoopie.twitchplugin.Data.DataManager;
import com.gitlab.quoopie.twitchplugin.Encryption.EncryptionManager;
import com.gitlab.quoopie.twitchplugin.Twirk.TwitchBot;

import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;

public class TwitchPlugin extends JavaPlugin {

    public final static String TWITCH_SETUP_PERMISSION = "twitchchat.setup";
    public final static String TWITCH_CHAT_PERMISSION = "twitchchat.chat";
    public final static String TWITCH_TOGGLE_PERMISSION = "twitchchat.toggle";

    @Getter
    private DataManager dataManager;

    @Getter
    private EncryptionManager encryptionManager;

    @Getter
    private TwitchBot twitchBot;

    @Override @SuppressWarnings("ConstantConditions")
    public void onEnable() {
        dataManager = new DataManager();
        encryptionManager = new EncryptionManager();

        twitchBot = new TwitchBot();

        boolean success = twitchBot.reload();

        if (!success) getLogger().log(Level.WARNING, "Unable to start twitch plugin fully. Please make sure it is fully configured!");

        getCommand("twitch").setExecutor(new TwitchCommand());
        getCommand("twitch").setTabCompleter(new TwitchTabCompleter());

        getCommand("twitchchat").setExecutor(new TwitchChatCommand());
        getCommand("twitchchat").setTabCompleter(new TwitchChatTabCompleter());

        getCommand("twitchchaton").setExecutor(new TwitchChatOnCommand());
        getCommand("twitchchaton").setTabCompleter(new TwitchChatOnTabCompleter());

        getCommand("twitchchatoff").setExecutor(new TwitchChatOffCommand());
        getCommand("twitchchatoff").setTabCompleter(new TwitchChatOffTabCompleter());
    }

    @Override
    public void onDisable() {
        if (twitchBot != null) twitchBot.getTwirk().close();
        twitchBot = null;
    }
}
