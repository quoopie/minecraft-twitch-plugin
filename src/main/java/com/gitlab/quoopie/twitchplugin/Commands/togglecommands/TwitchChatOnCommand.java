package com.gitlab.quoopie.twitchplugin.Commands.togglecommands;

import com.gitlab.quoopie.twitchplugin.Twirk.TwitchBot;
import com.gitlab.quoopie.twitchplugin.TwitchPlugin;
import com.gitlab.quoopie.twitchplugin.Utilites.Colorizer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class TwitchChatOnCommand implements CommandExecutor {
    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command cmd, @NotNull String label, @NotNull String[] args) {
        if (!sender.hasPermission(TwitchPlugin.TWITCH_TOGGLE_PERMISSION)) return true;

        TwitchPlugin twitchPlugin = TwitchPlugin.getPlugin(TwitchPlugin.class);
        TwitchBot twitchBot = twitchPlugin.getTwitchBot();
        Player player = ((Player) sender);

        twitchBot.getDisabledUsers().remove(player.getUniqueId().toString());
        twitchPlugin.getDataManager().getConfig().set(twitchPlugin.getDataManager().DISABLED_PATH, twitchBot.getDisabledUsers());

        Colorizer.sendMessage(sender, "&eTwitch chat is now on");

        return true;
    }
}
