package com.gitlab.quoopie.twitchplugin.Commands.twitchChat;

import com.gikk.twirk.Twirk;
import com.gitlab.quoopie.twitchplugin.Twirk.BotMode;
import com.gitlab.quoopie.twitchplugin.Twirk.TwitchBot;
import com.gitlab.quoopie.twitchplugin.TwitchPlugin;
import com.gitlab.quoopie.twitchplugin.Utilites.Colorizer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

public class TwitchChatCommand implements CommandExecutor {
    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command cmd, @NotNull String label, @NotNull String[] args) {
        TwitchPlugin plugin = TwitchPlugin.getPlugin(TwitchPlugin.class);
        TwitchBot twitchBot = plugin.getTwitchBot();
        Twirk twirk = twitchBot.getTwirk();
        StringBuilder messageBuilder = new StringBuilder();

        if (!sender.hasPermission(TwitchPlugin.TWITCH_CHAT_PERMISSION)) return true;

        if (args.length == 0) {
            Colorizer.sendMessage(sender, "&c/tc [message]");
            return true;
        }

        if (twitchBot.getBotMode().equals(BotMode.multi)) messageBuilder.append(sender.getName()).append(": ");

        for (String arg : args) {
            messageBuilder.append(arg).append(" ");
        }

        twirk.channelMessage(messageBuilder.toString());

        return true;
    }
}
